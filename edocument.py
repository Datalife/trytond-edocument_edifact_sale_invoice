# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

MESSAGE_INVOICE_VALUES = [
    ('INVOIC', 'INVOIC')
]


class EdocumentTemplate(metaclass=PoolMeta):
    __name__ = 'edocument.template'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.message_type.selection += MESSAGE_INVOICE_VALUES


class ConfigurationPath(metaclass=PoolMeta):
    __name__ = 'edocument.configuration.path'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.message_type.selection += MESSAGE_INVOICE_VALUES
